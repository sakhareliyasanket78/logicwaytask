<?php

namespace App\Http\Controllers;

use App\Models\File;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Spatie\PdfToText\Pdf;

class FileController extends Controller
{
    /**
     * @desc Display a listing of the file.
     *

     */
    public function index()
    {
        $data = File::get();
        return view('pages.FIle.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.FIle.create');
    }

    /**
     * @desc create & update file
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            // file validation
            $validation = Validator::make($request->all(), [
                'file' => 'required|mimes:pdf|max:2048',
            ]);

            if ($validation->fails()) {
                return response()->view('errors.' . '415', [], 415);
            } else {
                $word = 'proposal';
                $text = (new Pdf())
                    ->setPdf($request->file)
                    ->text();
                if (stripos($text, $word) !== false) {
                    DB::beginTransaction();

                    // file upload
                    $fileOriginalName = $request->file('file')->getClientOriginalName();
                    $fileSize = $request->file('file')->getSize();
                    $request->file->move(public_path('uploads/pdf'), $fileOriginalName);

                    // file data store in db
                    $file = File::updateOrCreate(
                        ['filename' => $fileOriginalName, 'filesize' => $fileSize],
                        ['filename' => $fileOriginalName, 'filesize' => $fileSize],
                    );
                    DB::commit();
                    return back()
                        ->with('success', 'You have successfully upload file.')
                        ->with('file', $fileOriginalName);
                } else {
                    return response()->view('errors.' . '422', [], 422);
                }
            }
        } catch (\Exception $e) {
            Session::flash('error', __('messages.invalidcode'));
            Session::flash('alert-class', __('messages.alertdanger'));
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
