<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Country;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CountryController extends Controller
{

    /**
     * @desc country wise details & all Recoard Details
     */
    public function index($id = null)
    {
        if ($id != null) {
            $country = Country::where('id', $id)->with('companys.users')->get();
            return json_encode($country);
        } else {
            $country = Country::with('companys.users')->get();
            return view('pages.Country.index', [
                'country' => $country
            ]);
        }
    }

    /**
     * @desc country data created
     */
    public function create()
    {
        return view('pages.Country.create');
    }

    /**
     * @desc country data created & updated
     */
    public function store(Request $request)
    {
        try {
            // validation
            $validation = \Illuminate\Support\Facades\Validator::make($request->all(), [
                'country' => 'required',
                'company' => 'required',
                'user' => 'required',
            ]);
            // check validation
            if ($validation->fails()) {
                Session::flash('error', __('messages.invalidvalidation'));
                Session::flash('alert-class', __('messages.alertdanger'));
                return redirect()->back();
            } else {
                DB::beginTransaction();
                // insert & update country
                $country = Country::updateOrCreate(
                    ['name' => $request->country],
                    ['name' => $request->country]
                );
                // country id
                $countryId = $country->id;

                //insert or update company
                $company = Company::updateOrCreate(
                    ['country_id' => $countryId, 'name' => $request->company],
                    ['country_id' => $countryId, 'name' => $request->company],
                );

                //insert or update users
                $user = User::updateOrCreate(
                    ['name' => $request->user],
                    ['name' => $request->user],
                );

                $company->users()->attach($user);
                DB::commit();
                Session::flash('success', __('messages.dataRagister'));
                Session::flash('alert-class', __('messages.alertsuccuess'));

                return redirect()->route('country.index');
            }
        } catch (\Exception $e) {
            Session::flash('error', __('messages.invalidcode'));
            Session::flash('alert-class', __('messages.alertdanger'));
            return redirect()->back();
        }
    }

}
