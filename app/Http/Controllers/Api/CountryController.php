<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Country;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CountryController extends Controller
{

    /**
     * @desc display countrywiserecoard
     */
    public function displayCountryWiseRecoard($id)
    {
        try {
            $country = Country::where('id', $id)->with('companys.users')->get();
            if ($country && $id == !null) {
                return \response()->json([
                    "message" => "recoard display successfully",
                    'allRecordDetails' => $country,
                ], 201);
            } else {
                return \response()->json([
                    "message" => "recoard not found",
                ], 500);
            }


        } catch (\Exception $e) {
            return \response()->json([
                "message" => "invalid Code",
            ], 500);
        }

    }

    /**
     * @desc display all recoard
     */
    public function displayAllRecoard()
    {
        try {
            $country = Country::with('companys.users')->get();
            if ($country) {
                return \response()->json([
                    "message" => "recoard display successfully",
                    'allRecordDetails' => $country,
                ], 201);
            } else {
                return \response()->json([
                    "message" => "recoard not found",
                ], 500);
            }

        } catch (\Exception $e) {
            return \response()->json([
                "message" => "invalid Code",
                'error' => $e
            ], 500);
        }

    }

    /**
     * @desc country data created & updated
     */

    public function store(Request $request)
    {
        try {
            // validation
            $validation = \Illuminate\Support\Facades\Validator::make($request->all(), [
                'country' => 'required',
                'company' => 'required',
                'user' => 'required',
            ]);
            // check validation
            if ($validation->fails()) {
                return response()->json([
                    'message' => 'validation error',
                    'errors' => $validation->errors()], 400);
            } else {
                DB::beginTransaction();
                // insert & update country
                $country = Country::updateOrCreate(
                    ['name' => $request->country],
                    ['name' => $request->country]
                );
                // country id
                $countryId = $country->id;

                //insert or update company
                $company = Company::updateOrCreate(
                    ['country_id' => $countryId, 'name' => $request->company],
                    ['country_id' => $countryId, 'name' => $request->company],
                );

                //insert or update users
                $user = User::updateOrCreate(
                    ['name' => $request->user],
                    ['name' => $request->user],
                );

                $company->users()->attach($user);
                DB::commit();
                return \response()->json([
                    "message" => "recoard create successfully",
                    'country' => $country,
                    'company' => $company,
                    'user' => $user,
                ], 201);

            }
        } catch (\Exception $e) {
            return \response()->json([
                "message" => "invalid Code",
            ], 500);
        }
    }
}
