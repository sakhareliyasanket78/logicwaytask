<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;

    protected $table = 'countries';
    protected $fillable = ['name'];

    /**
     * relation between country & company
     */
    public function companys()
    {
        return $this->hasMany(Company::class);
    }
}
