<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;
    protected $fillable=['country_id','name','description'];

    /**
     * The User that belong to the Company .
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    }
    /**
     * relation between country & company
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
