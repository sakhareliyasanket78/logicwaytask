@extends('layout.app')
@section('title', 'FileListing')

@section('header-script')
@endsection

@section('middle-content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left text-center">
                <h2>File Details</h2>
            </div>
            <div class="text-right">
                <a class="btn btn-success" href="{{route('file.create')}}" title="Create a product">Create<i
                        class="fas fa-plus-circle"></i>
                </a>
            </div>
        </div>
    </div>
    <br>
    <table class="table table-bordered table-striped">
        <tr>
            <th width="10%">FIle</th>
            <th width="35%">size</th>
        </tr>
        @forelse($data as $row)
            <tr>
                <td><a href="{{asset('uploads/pdf/'.$row->filename)}}" target="_blank">{{ $row->filename }}</a></td>
                <td>{{ $row->filesize }}</td>
            </tr>
        @empty
            <h1 class="text-center">recoard is Not Found </h1>
        @endforelse
    </table>

@endsection
@section('footer-script')
@endsection
