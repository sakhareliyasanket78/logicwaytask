@extends('layout.app')
@section('title', 'Add Details')

@section('header-script')
    <script src="{{asset('assets/js/jquery.validate.min.js')}}"></script>
@endsection

@section('middle-content')
    <div class="row">
        <div class="col-lg-12 margin-tb ">
            <div class="pull-left text-center">
                <h2>Add File Details</h2>
            </div>
            <div class="text-right">
                <a class="btn btn-primary" href="{{route('file.index')}}" title="Create a product"><i
                        class="fas fa-plus-circle">Back</i>
                </a>
            </div>
        </div>
    </div>
    <form action="{{route('file.submit')}}" method="POST" id="pdfffile" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="email">File</label>
            <input type="file" name="file" class="form-control" id="file">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
@section('footer-script')
    <script>
        $(document).ready(function () {
            $('#pdfffile').validate({
                rules: {
                    file: {
                        required: true,
                    },
                },
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });
        });
    </script>
@endsection
