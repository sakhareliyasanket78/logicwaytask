@extends('layout.app')
@section('title', 'Add Details')

@section('header-script')
    <script src="{{asset('assets/js/jquery.validate.min.js')}}"></script>
@endsection

@section('middle-content')
    <div class="row">
        <div class="col-lg-12 margin-tb ">
            <div class="pull-left text-center">
                <h2>Create Details</h2>
            </div>
            <div class="text-right">
                <a class="btn btn-primary" href="{{route('country.index')}}" title="Create a product"><i
                        class="fas fa-plus-circle">Back</i>
                </a>
            </div>
        </div>
    </div>
    <form action="{{route('country.submit')}}" method="POST" id="country">
        @csrf
        <div class="form-group">
            <label for="email">Country</label>
            <input type="text" class="form-control" id="country" name="country">
        </div>
        <div class="form-group">
            <label for="company">Company</label>
            <input type="text" class="form-control" id="company" name="company">
        </div>
        <div class="form-group">
            <label for="user">User</label>
            <input type="text" class="form-control" id="user" name="user">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
@section('footer-script')
    <script>
        $(document).ready(function () {
            $('#country').validate({
                rules: {
                    country: {
                        required: true,
                    },
                    company: {
                        required: true,
                    },
                    user: {
                        required: true,
                    },
                },
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });
        });
    </script>
@endsection
