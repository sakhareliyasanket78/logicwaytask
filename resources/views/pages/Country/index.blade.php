@extends('layout.app')
@section('title', 'CountryWiseData')

@section('header-script')
@endsection

@section('middle-content')
    <div class="row ">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left text-center">
                <h2>Country Wise Details</h2>
            </div>
            <div class="text-right">
                <a class="btn btn-success" href="{{route('country.create')}}" title="Create a product">Create<i
                        class="fas fa-plus-circle"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="country">Select your Country</label>
        <select name="country" id="country" class="form-control">
            <option value="">Select Country</option>
            @foreach($country as $key => $value)
                <option value="{{$value->id}}">{{$value->name}}</option>
            @endforeach
        </select>
    </div>
    <div>
        <div class="treeview w-20 border">
            <h6 class="pt-3 pl-3">Country Wise Details</h6>
            <hr>
            <div class="contrywisedetails">
                @foreach($country as $countryList)
                    <ul class="mb-1 pl-3 pb-2">
                        <li><i class="fas fa-angle-right rotate"></i>
                            <span><i class="far fa-envelope-open ic-w mx-1 "></i><b>Country:-</b>  <span
                                    class="country">{{$countryList->name}}</span></span>
                            @foreach($countryList->companys as $countryWiseCompany)
                                <ul class="nested">
                                    <li><i class="fas fa-angle-right rotate"></i>
                                        <span><i class="far fa-calendar-alt ic-w mx-1"></i><b>Company:-</b>{{$countryWiseCompany->name}} </span>
                                        @foreach($countryWiseCompany->users as $user)
                                            <ul class="nested">
                                                <li>
                                                    <i class="far fa-clock ic-w mr-1"></i><b>User:-</b>{{$user->name}}
                                                    <i class="far fa-clock ic-w mr-1"></i><b>Date:-</b>{{date('d/m/Y', strtotime($user->created_at))  }}
                                                </li>
                                            </ul>
                                        @endforeach
                                    </li>
                                </ul>
                            @endforeach

                        </li>
                    </ul>
                @endforeach
            </div>
        </div>
        <div>
        </div>
    </div>
@endsection
@section('footer-script')
    <script>

        $(document).ready(function () {
            $('select[name="country"]').on('change', function () {
                var country_id = $(this).val();
                if (country_id) {
                    $.ajax({
                        url: "{{url('/country/')}}/" + country_id,
                        type: "GET",
                        dataType: "json",
                        success: function (data) {

                            var html =
                                '<ul class="mb-1 pl-3 pb-2">'
                                + '<li>'
                                + '<i class="fas fa-angle-right rotate"></i>'
                                + '<span><i class="far fa-envelope-open ic-w mx-1 "></i><b>Country:-</b>  <span class="country">'
                                + data[0].name
                                + '</span></span>'
                                + '<ul class="nested " id="yellowTeamList">'
                                + '</ul>'
                                + '</li>'
                                + '</ul>'
                            ;
                            $('.contrywisedetails').html(html);
                            var companyDetails = data[0].companys;
                            for (var i = 0; i < companyDetails.length; i++) {
                                $('#yellowTeamList').append('<li> <span class=""><i class="far fa-calendar-alt ic-w mx-1"></i><b>Company:-</b> <span >"' + companyDetails[i].name + '"</span> </span> <ul class="nested " id="userDetails' + i + '"></ul> </li>');
                                for (var j = 0; j < companyDetails[i].users.length; j++) {
                                    // set user id in companywise

                                    var idName = 'userDetails';
                                    var ParentVal = i;
                                    var fullName = idName.concat(ParentVal);

                                    // date get dd-mm-yyyy format
                                    let d = new Date(companyDetails[i].users[j].created_at);
                                    var month = d.getMonth() + 1;
                                    var day = d.getDate();

                                    var date =
                                        (day < 10 ? '0' : '') + day + '/' +
                                        (month < 10 ? '0' : '') + month + '/' +
                                        d.getFullYear();
                                    $('#' + fullName).append('<li> <i class="far fa-clock ic-w mr-1"></i> <b>User:-</b> "' + companyDetails[i].users[j].name + '" <i class="far fa-clock ic-w mr-1"></i><b>Date:-</b>"' + date + '" </li>');
                                }

                            }

                        }
                    });
                }
            });
        });
    </script>
@endsection
