<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
{{--    <script src="{{asset('assets/js/jquery.validate.min.js')}}"></script>--}}
    <script src="{{asset('assets/js/jquery-3.3.1.slim.min.js')}}"></script>
    <script src="{{asset('assets/js/popper.min.js')}}"></script>

    @yield('header-script')
</head>

<body>
<div id="app">
    <!-- Header -->
@include('layout.header')
<!-- /Header -->
    <div class="page-content">

        <div class="content-wrapper bg_app_content_color">
            <div class="content" style="padding: 1em;margin: 0 16px">
                @if(\Illuminate\Support\Facades\Session::has('success'))
                    <p class="alert {{\Illuminate\Support\Facades\Session::get('alert-class', 'alert-info')}} alert-dismissible"
                       id="time">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{ \Illuminate\Support\Facades\Session::get('success') }}
                    </p>
                @elseif(\Illuminate\Support\Facades\Session::has('error'))
                    <p class="alert {{\Illuminate\Support\Facades\Session::get('alert-class', 'alert-info')}} alert-dismissible"
                       id="time">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{ \Illuminate\Support\Facades\Session::get('error') }}
                    </p>
                @endif
                @yield('middle-content')
            </div>
            <!-- Footer -->
        @include('layout.footer')
        <!-- /Footer -->
        </div>
    </div>
</div>
@yield('footer-script')
</body>
</html>
