<?php

return [
    'userUpdated' => 'User Successfully Updated.',
    'RecordNotFound' => 'Record not found.',
    'serverError' => 'Temporary server error. Try again.',
    'dataRagister'=>'Country data Successfully Ragister',
    'alertdanger'=>'alert-danger',
    'alertsuccuess'=>'alert-success',
    'invalidcode'=>'Invalid  code',
    'invalidvalidation'=>'Invalid  Validation',
];
