<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/all_country_details', [\App\Http\Controllers\Api\CountryController::class, 'displayAllRecoard']);
Route::get('/country_wise_details/{id}', [\App\Http\Controllers\Api\CountryController::class, 'displayCountryWiseRecoard']);
Route::post('/country_create', [\App\Http\Controllers\Api\CountryController::class, 'store']);
