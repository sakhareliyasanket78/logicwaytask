<?php

namespace Database\Seeders;

use App\Models\Country;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = array(
            array(
                'id' => 1,
                'name' => 'United States'
            ),
            array(
                'id' => 2,
                'name' => 'Canada'
            ),
            array(
                'id' => 3,
                'name' => 'Afghanistan'
            ),
            array(
                'id' => 4,
                'name' => 'Albania'
            ),
            array(
                'id' => 5,
                'name' => 'Algeria'
            ),
            array(
                'id' => 6,
                'name' => 'American Samoa'
            ),
            array(
                'id' => 7,
                'name' => 'Andorra'
            ),
            array(
                'id' => 8,
                'name' => 'Angola'
            ),
            array(
                'id' => 9,
                'name' => 'Anguilla'
            ),
            array(
                'id' => 10,
                'name' => 'Antarctica'
            ),
            array(
                'id' => 11,
                'name' => 'India'
            ),
        );
        DB::table('country')->insert($countries);

    }
}
